//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.
//For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90,
//false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo()
//that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc()
//that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

// Instruction 1
let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],
  // Instruction 2
  computeAverage() {
    let sum = 0;
    let average = 0;
    this.grades.forEach((item) => {
      sum += item;
    });
    average = sum / this.grades.length;
    return average;
  },
  // Instruction 3
  willPass() {
    if (this.computeAverage() >= 85) {
      return true;
    } else {
      return false;
    }
  },

  // Instruction 4
  willPassWithHonors() {
    if (this.computeAverage() >= 90) {
      return true;
    } else if (this.computeAverage() >= 85 && this.computeAverage() < 90) {
      return false;
    } else if (this.computeAverage() < 85) {
      return undefined;
    }
  },
};

// Instruction 1
let studentTwo = {
  name: "Joe",
  email: "joe@mail.com",
  grades: [78, 82, 79, 85],
  // Instruction 2
  computeAverage() {
    let sum = 0;
    let average = 0;
    this.grades.forEach((item) => {
      sum += item;
    });
    average = sum / this.grades.length;
    return average;
  },
  // Instruction 3
  willPass() {
    if (this.computeAverage() >= 85) {
      return true;
    } else {
      return false;
    }
  },
  // Instruction 4
  willPassWithHonors() {
    if (this.computeAverage() >= 90) {
      return true;
    } else if (this.computeAverage() >= 85 && this.computeAverage() < 90) {
      return false;
    } else if (this.computeAverage() < 85) {
      return undefined;
    }
  },
};

// Instruction 1
let studentThree = {
  name: "Jane",
  email: "jane@mail.com",
  grades: [87, 89, 91, 93],
  // Instruction 2
  computeAverage() {
    let sum = 0;
    let average = 0;
    this.grades.forEach((item) => {
      sum += item;
    });
    average = sum / this.grades.length;
    return average;
  },
  // Instruction 3
  willPass() {
    if (this.computeAverage() >= 85) {
      return true;
    } else {
      return false;
    }
  },
  // Instruction 4
  willPassWithHonors() {
    if (this.computeAverage() >= 90) {
      return true;
    } else if (this.computeAverage() >= 85 && this.computeAverage() < 90) {
      return false;
    } else if (this.computeAverage() < 85) {
      return undefined;
    }
  },
};

// Instruction 1
let studentFour = {
  name: "Jessie",
  email: "jessie@mail.com",
  grades: [91, 89, 92, 93],
  // Instruction 2
  computeAverage() {
    let sum = 0;
    let average = 0;
    this.grades.forEach((item) => {
      sum += item;
    });
    average = sum / this.grades.length;
    return average;
  },
  // Instruction 3
  willPass() {
    if (this.computeAverage() >= 85) {
      return true;
    } else {
      return false;
    }
  },
  // Instruction 4
  willPassWithHonors() {
    if (this.computeAverage() >= 90) {
      return true;
    } else if (this.computeAverage() >= 85 && this.computeAverage() < 90) {
      return false;
    } else if (this.computeAverage() < 85) {
      return undefined;
    }
  },
};

// Instruction 5
let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],
  // Instruction 6
  countHonorStudents() {
    let honorStudentCount = 0;
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        honorStudentCount += 1;
      }
    });
    console.log(honorStudentCount);
  },
  // Instruction 7
  honorsPercentage() {
    let honorStudentCount = 0;
    let student = this.students.length;
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        honorStudentCount += 1;
      }
    });
    console.log((honorStudentCount / student) * 100);
  },
  // Instruction 8
  retrieveHonorStudentInfo() {
    const honorStudents = this.students
      .map((student) => {
        return {
          email: student.email,
          aveGrade: student.computeAverage(),
        };
      })
      .filter((item) => {
        return item.aveGrade >= 90;
      });
    console.log(honorStudents);
  },
  // Instruction 9
  sortHonorStudentsByGradeDesc() {
    const honorStudents = this.students
      .map((student) => {
        return {
          email: student.email,
          aveGrade: student.computeAverage(),
        };
      })
      .filter((item) => {
        return item.aveGrade >= 90;
      })
      .sort((a, b) => {
        return b.aveGrade - a.aveGrade;
      });
    console.log(honorStudents);
  },
};
