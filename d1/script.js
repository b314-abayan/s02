// use an object literal: {} to create an object representing a user
// encapsulation
// whenever we add properties or methos to an object, we are performing excapsulation
// the organization of information (as properties) and behaivior (as methods) to belong to the object that encapsulates them
// the scope of encapsulation is denoted byobject literals

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],
  //  methods
  // add the fuctionalities available to a student as object methods
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },

  //Mini-activity
  averageGrade() {
    let sum = 0;
    let average = 0;
    this.grades.forEach((item) => {
      sum += item;
    });
    average = sum / this.grades.length;
    return average;
  },
  willPass() {
    if (this.averageGrade() >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    if (this.willPass() && this.averageGrade >= 90) {
      return true;
    } else {
      return false;
    }
  },
};

// log the content of studentOne encapsulated information in the console
// the keyword "this refers to the object encapsulation the method where "this" is called"

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade averages are ${studentOne.grades}`);

console.log(studentOne);
console.log(`student one's average grade is ${studentOne.averageGrade()}`);
console.log(studentOne.willPass());
console.log(studentOne.willPassWithHonors());
